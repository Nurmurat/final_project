package kz.aitu.advancedJava.startingPoint.services;

import kz.aitu.advancedJava.startingPoint.model.OrderItem;
import kz.aitu.advancedJava.startingPoint.model.Product;
import kz.aitu.advancedJava.startingPoint.repository.OrderItemRepository;
import kz.aitu.advancedJava.startingPoint.repository.ProductRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.List;
import java.util.PrimitiveIterator;

@Service
public class OrderItemService {
    private final OrderItemRepository orderItemRepository;
    private final ProductRepository productRepository;


    public OrderItemService(OrderItemRepository orderItemRepository,ProductRepository productRepository) {
        this.orderItemRepository = orderItemRepository;
        this.productRepository = productRepository;
    }

    public void createOrderItems(long order_id, List<OrderItem> orderItems){
        for(int i = 0; i < orderItems.size(); i++){
            orderItemRepository.insertOrderItem(orderItems.get(i).getProductId(),
                    orderItems.get(i).getProductName(),
                    orderItems.get(i).getQuantity(),
                    orderItems.get(i).getPrice(),
                    orderItems.get(i).getOrder_date(),
                    order_id,
                    "available");
            System.out.println(orderItems.get(i).getProductName());
        }
    }

    public List<OrderItem> findAllByOrderId(long orderId) {return orderItemRepository.findAllByOrderId(orderId); }


    public List<OrderItem> createDashboard() {
//        List<OrderItem> orderItemList = (List) orderItemRepository.findAll();
//        List<Product> products = (List) productRepository.findAll();
//        List<OrderItem> dashboard = new ArrayList<>();
//        int quantity = 0;
//        for (int j = 0; j < products.size(); j++){
//            for (int i = 1; i < orderItemList.size(); i++){
//                if (products.get(j).getId() == orderItemList.get(i).getProductId())
//                    quantity += orderItemList.get(i).getQuantity();
//                }
//                dashboard.add(orderItemList.get(i));
//                dashboard.get()
//            }
//        return dashboard;

        List<OrderItem> dashboard = new ArrayList<>();
        List<Product> products = (List) productRepository.findAll();
        int size = products.size();
        int[] array = new int[size];
        for (int i = 0; i < size; i++){
            array[i] = 0;
        }
        List<OrderItem> orderItemList = (List) orderItemRepository.findAll();
        for(int i=0; i< orderItemList.size(); i++){
            long a = orderItemList.get(i).getProductId()-1;
            array[(int) a] += orderItemList.get(i).getQuantity();
        }
        for (int i=0; i < size; i++){
            if (array[i]>0){
                OrderItem orderItem = new OrderItem();
                orderItem.setProductId(i+1);
                orderItem.setName(products.get(i).getName());
                orderItem.setQuantity(array[i]);
                orderItem.setPrice(orderItem.getQuantity()*products.get(i).getPrice());
                dashboard.add(orderItem);
            }
        }
        return dashboard;

    }

    public void deleteFromDashboard(Long orderId) {
        orderItemRepository.deleteById(orderId);
    }
}
