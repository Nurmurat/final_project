package kz.aitu.advancedJava.startingPoint.repository;

import kz.aitu.advancedJava.startingPoint.model.OrderItem;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Repository
public interface OrderItemRepository extends CrudRepository<OrderItem, Long>{
    @Transactional
    @Modifying
    @Query(value = "insert into orderitem (product_id, product_name,quantity,price,order_date, customer_order_id,status) values(:product_id,:product_name,:quantity,:price,:order_date,:customer_order_id,:status)", nativeQuery = true )
    void insertOrderItem(@Param("product_id") long product_id, @Param("product_name") String product_name, @Param("quantity") int quantity, @Param("price") int price, @Param("order_date") Date order_date, @Param("customer_order_id") long customer_order_id, @Param("status") String status);

    List<OrderItem> findAllByOrderId(long orderId);
}
