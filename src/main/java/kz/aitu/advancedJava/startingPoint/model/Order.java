package kz.aitu.advancedJava.startingPoint.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name= "customers_orders")
public class Order {
    public Order(Long customerId, LocalDate date, int totalPrice, String status){
        this.customerId = customerId;
        this.date = date;
        this.totalPrice = totalPrice;
        this.status = status;
    }

    @Id
    private long id;
    private Long customerId;
    private LocalDate date;
    private int totalPrice;
    private String status;

    public int getTotalPrice() {
        return totalPrice;
    }

    public LocalDate getDate() {
        return date;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public String getStatus() {
        return status;
    }

    public long getId() {
        return id;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
