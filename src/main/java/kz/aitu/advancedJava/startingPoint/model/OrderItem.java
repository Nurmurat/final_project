package kz.aitu.advancedJava.startingPoint.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "orderitem")
public class OrderItem {
    @Id
    private long id;
    private Long productId;
    @Column(name = "product_name")
    private String name;
    private int quantity;
    private int price;
    private Date order_date;
    @Column(name = "customer_order_id")
    private Long orderId;
    private String status;

    public long getId() {
        return id;
    }

    public int getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public Long getProductId() {
        return productId;
    }

    public String getProductName() {
        return name;
    }

    public String getStatus() {
        return status;
    }

    public void setProductId(int id) { this.productId = (long) id; }
}
