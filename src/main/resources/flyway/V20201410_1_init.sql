DROP TABLE  IF EXISTS products;
CREATE TABLE products (
    id        serial CONSTRAINT products_primary PRIMARY KEY,
    name       varchar(40) NOT NULL,
    price      int4 NOT NULL,
    description  varchar(255) NOT NULL,
    category_id  int8 NOT NULL,
    barcode      varchar(32) NOT NULL UNIQUE
);
DROP TABLE  IF EXISTS categories;
CREATE TABLE categories (
    id        serial CONSTRAINT categories_primary PRIMARY KEY,
    name       varchar(40) NOT NULL,
    description  varchar(255) NOT NULL
);

DROP TABLE  IF EXISTS auth;
CREATE TABLE auth (
  id serial CONSTRAINT auth_primary PRIMARY KEY,
  login varchar(255) NOT NULL,
  password varchar(13) NOT NULL,
  customer_id int8 NOT NULL,
  token varchar (255)
);
create unique index auth_token_uindex
	on auth (token);

DROP TABLE IF EXISTS customer;
CREATE TABLE customer(
  id serial CONSTRAINT customer_primary PRIMARY KEY,
  name varchar(255) NOT NULL,
  address varchar(255) NOT NULL,
  phone varchar(100) NOT NULL,
  email varchar(255) NOT NULL,
  login varchar(255) NOT NULL,
  password varchar(255) NOT NULL
);
CREATE TABLE order_history (
                               id serial CONSTRAINT order_history_primary PRIMARY KEY,
                               customer_order_id integer NOT NULL,
                               date varchar(255) NOT NULL
);

CREATE TABLE employee (
                          id      serial CONSTRAINT employee_primary PRIMARY KEY,
                          name    varchar(50) NOT NULL,
                          lastname    varchar(50) NOT NULL,
                          depid    serial,
                          phone    varchar(15) NOT NULL,
                          address varchar(100) NOT NULL,
                          iin     varchar(12) NOT NULL,
                          email  varchar(70) NOT NULL
);
DROP TABLE IF EXISTS department;
CREATE TABLE department (
                            id serial CONSTRAINT department_primary PRIMARY KEY,
                            name varchar(255)  NOT NULL,
                            category_id serial NOT NULL,
                            description varchar(255) NOT NULL
);


DROP TABLE IF EXISTS orderItem;
CREATE TABLE orderItem (
                           id serial CONSTRAINT orderItem_primary PRIMARY KEY,
                           product_id integer NULL,
                           product_name varchar(255) NULL,
                           quantity integer NULL,
                           price varchar(100) NOT NULL,
                           customer_order_id integer NULL,
                           status varchar(255) NOT NULL
);


DROP TABLE IF EXISTS customers_orders;
CREATE TABLE customer_order(
                               id serial CONSTRAINT cust_order_primary PRIMARY KEY,
                               customer_id integer NOT NULL,
                               date varchar(255),
                               total_price varchar(100) NOT NULL,
                               made_status varchar(255) NOT NULL,
                               delivered_status varchar(255) NOT NULL
);

CREATE TABLE employee (
    id      serial CONSTRAINT employee_primary PRIMARY KEY,
    name    varchar(50) NOT NULL,
    lastname    varchar(50) NOT NULL,
    depid    serial,
    phone    varchar(15) NOT NULL,
    address varchar(100) NOT NULL,
    iin     int NOT NULL,
    email  varchar(70) NOT NULL
);
DROP TABLE IF EXISTS department;
CREATE TABLE department (
  id SERIAL CONSTRAINT department_primary PRIMARY KEY,
  name varchar(255)  NOT NULL,
  category_id serial NOT NULL,
  description varchar(255) NOT NULL
);



DROP TABLE IF EXISTS customers_orders;
CREATE TABLE customers_orders(
  id SERIAL PRIMARY KEY,
  customer_id integer NOT NULL,
  date varchar(255),
  total_price varchar(100) NOT NULL,
  status varchar(255) NOT NULL
);

DROP TABLE IF EXISTS order_history;
CREATE TABLE order_history(
  id SERIAL PRIMARY KEY,
  customer_order_id integer NOT NULL,
  date varchar(255),
  status varchar (50)
);





