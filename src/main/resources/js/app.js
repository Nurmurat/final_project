var app = angular.module('aitu-project', []);

app.controller('ProductCtrl', function($scope, $http) {
    $scope.auth = {
        login: '',
        password: '',
        token: ''
    };

    $scope.customer = {};

    $scope.login = function(auth) {
        $http({
            url: 'http://127.0.0.1:8081/login',
            method: "POST",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            },
            data: auth
        })
            .then(function (response) {
                    $scope.auth = response.data;
                    $scope.getMe();
                    $scope.getMyOrders();
                },
                function (response) { // optional
                    $scope.auth = {}
                });
    };

    $scope.customerMessage = '';
    $scope.getMe = function(){
        $http({
            url: 'http://127.0.0.1:8081/customers/me',
            method: "GET",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json",
                "Auth":$scope.auth.token
            }
        })
            .then(function (response) {
                    $scope.customer = response.data;
                },
                function (response) { // optional
                    console.log(response);
                    $scope.customer ={};
                    $scope.shopMessage = 'Login or Password is incorrect!';
                });
    };

    $scope.productList = [];
    $scope.categoryList = [];

    $scope.getProducts = function() {
        $http({
            url: 'http://127.0.0.1:8081/api/products',
            method: "GET",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }
        })
            .then(function (response) {
                    console.log(response);
                    $scope.productList = response.data;
                },
                function (response) { // optional
                    console.log(response);
                });
    };

    $scope.getProductsByCategory = function(categoryID) {
        $http({
            url: 'http://127.0.0.1:8081/api/products/category/' + categoryID,
            method: "GET",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }
        })
            .then(function (response) {
                    console.log(response);
                    $scope.productList = response.data;
                },
                function (response) { // optional
                    console.log(response);
                });
    };

    $scope.getCategories = function() {
        $http({
            url: 'http://127.0.0.1:8081/api/categories',
            method: "GET",
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }
        })
            .then(function (response) {
                    console.log(response);
                    $scope.categoryList = response.data;
                },
                function (response) { // optional
                    console.log(response);
                });
    };

    $scope.getCategories();
    $scope.getProducts();

    $scope.orderItemList = [];

    $scope.add = function(product){
        for(orderItem of $scope.orderItemList) {
            if(orderItem.productId === product.id) {
                orderItem.name = product.name;
                orderItem.price = product.price;
                orderItem.quantity += 1;
                return;
            }
        }
        $scope.orderItemList.push({productId: product.id, name: product.name,price: product.price, quantity: 1});
    };


    $scope.orderMessage = '';

    $scope.sendOrder = function() {
        if (Object.keys($scope.customer).length === 0) {
            $scope.orderMessage = 'Authorize!';
        } else {
            if (Object.keys($scope.orderItemList).length === 0) {
                $scope.orderMessage = 'Empty order!';
            } else {
                $scope.orderMessage = 'Your order is accepted!';
                $scope.createCustomerOrder();
                $scope.getMyOrders();
                console.log($scope.customer);
            }
        }
    };

    $scope.createCustomerOrder = function () {
        $http({
            url: 'http://127.0.0.1:8081/create-order',
            method: 'POST',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json",
                "auth": $scope.auth.token
            },
            data: $scope.orderItemList
        }).then(function (response){
            console.log(response);
            }, function (response){
            console.log(response);
        })
    };

    $scope.myOrders = [];

    $scope.getMyOrders = function () {
        $http({
            url: 'http://127.0.0.1:8081/get-my-orders',
            method: 'GET',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json",
                "auth": $scope.auth.token
            }
        }).then(function (response){
            console.log(response);
            $scope.myOrders = response.data;
        }, function (response){
            console.log(response);
        })
    };

    $scope.myOrderItems = [];

    $scope.getMyOrderItems = function (orderId) {
        $http({
            url: 'http://127.0.0.1:8081/get-my-orderitems/' + orderId,
            method: 'GET',
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Content-Type": "application/json"
            }
        }).then(function (response){
            console.log(response);
            $scope.myOrderItems = response.data;
        }, function (response){
            console.log(response);
        })
    };
        });


